import java.util.*;

public class JavaCollectionAssignment {
    public static void main(String[] args) {
        System.out.println("--List--");
        List lists = new ArrayList();
        lists.add("Darrel");
        lists.add("Annabel");
        lists.add("Dare");
        lists.add("Adewale");
        lists.add("Ojo");
        lists.add("Dare");
        lists.add("Feyisikemi");
        lists.add("Oluwadare");

        for (int i = 0; i < lists.size(); i++) {
            System.out.println(lists.get(i));
        }
        System.out.println("--Set--");
        Set set = new HashSet();
        set.add("Darrel");
        set.add("Annabel");
        set.add("Dare");
        set.add("Adewale");
        set.add("Ojo");
        set.add("Dare");
        set.add("Feyiskemi");
        set.add("Oluwaadare");
        for (Object str : set) {
            System.out.println((String) str);
        }
        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        queue.add("Darrel");
        queue.add("Annabel");
        queue.add("Dare");
        queue.add("Adewale");
        queue.add("Ojo");
        queue.add("Dare");
        queue.add("Feyiskemi");
        queue.add("Oluwaadare");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());

        }
        System.out.println("-- Map --");
        Map map = new HashMap();
        map.put(1,"Darrel");
        map.put(2,"Annabel");
        map.put(3,"Dare");
        map.put(4,"Adewale");
        map.put(5,"Ojo");
        map.put(3,"Dare");
        map.put(7,"Feyiskemi");
        map.put(8,"Oluwadare");

        for (int i = 1; i < 9; i++) {
            String result = (String) map.get(i);
            System.out.println(result);

        }
    }
}